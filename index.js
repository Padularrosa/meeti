const express = require('express');
const router = require('./routes');
const expressLayouts = require('express-ejs-layouts');
const path = require('path');
const bodyParser = require('body-parser');
const flash = require('connect-flash');
const session = require('express-session');
const cookieParser = require('cookie-parser');

//configuracion BD
const db = require('./config/db');
require('./models/Usuarios');
db.sync().then(() => console.log('DB connect')).catch((error) => console.log(error));

//variables de desarrollo
require('dotenv').config({path: 'variables.env'});

//aplicacion principal
const app = express();

//parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

//EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

//Ubicacion
app.set('views', path.join(__dirname, './views'));

//Estaticos
app.use(express.static('public'));

//Habilitar cookie parser
app.use(cookieParser());



//flash
app.use(flash());

app.use(session({
    secret: process.env.SECRETO,
    key: process.env.KEY,
    resave: false,
    saveUninitialized: false
}));

app.use((req,res,next)=>{
    res.locals.mensajes= req.flash();
    const fecha = new Date();
    res.locals.year = fecha.getFullYear();
    next();

});

//Routing
app.use('/', router());
//Agrega el puerto

app.listen(process.env.PORT, ()=> {
    console.log('server is running');
})
